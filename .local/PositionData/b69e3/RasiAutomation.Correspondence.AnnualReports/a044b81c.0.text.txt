State of Rhode Island N Department of State ☒ siness Services Division Nellie M. Gorbea, Secretary of State UOPE

July 28, 2021

Absalona Hill Road Solar, LLC c/o REGISTERED AGENT SOLUTIONS, INC. 222 JEFFERSON BOULEVARD, SUITE 200 WARWICK, RI 02888

ENTITY ID: 001698721 ENTITY NAME: Absalona Hill Road Solar, LLC

Dear Sir or Madam: This is a reminder that your 2021 Annual Report will soon be due.
State law requires you to file an Annual Report along with the $50 filing fee with the Department of State between September 1 and November 1.
There are three easy ways to file:

1. File online with a credit card at www.sos.ri.gov/divisions/business-services - (File Online), The following unique Customer Identification Number (CID) and Pin Number will be required:

CUSTOMER ID NUMBER (CID): mz4jj0 PIN: 7563

2. Mail in your completed Annual Report form that is available at vww.sos.ri.gov/divisions/business-services (Under Quick Links click "Forms Library" - Search Form 632) along with a check or money order for $50 payable to "Department of State".

3. File your Annual Report in person at our office located at 148 West River Street, Providence, RI

The filing of the annual report is a mandatory requirement to ensure good standing in this office and to avoid revocation proceedings.
In accordance with state law, limited liability companiestha fail to file an Annual Report by December 1, 2021 are subject to a penalty fee of $25.

Sincerely,

Nellie M. Gorbea Secretary of State

148 W. River Street, Providence, RI 02904-2615 Phone: 401-222-3040 Fax: 401-222-1309 corporations@sos.ri.gov www.sos.ri.gov